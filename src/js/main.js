// // Add main scripts here
// window.onload = function() {
//
//   var placeholder = document.querySelector('.placeholder'),
//       small = placeholder.querySelector('.img-small');
//
//   // 1: load small image and show it
//   var img = new Image();
//   img.src = small.src;
//   img.onload = function () {
//    small.classList.add('loaded');
//   };
//
//   // 2: load large image
//   var imgLarge = new Image();
//   imgLarge.src = placeholder.dataset.large;
//   imgLarge.onload = function () {
//     small.classList.remove('loaded');
//     imgLarge.classList.add('loaded');
//   };
//   placeholder.appendChild(imgLarge);
// };

// flag : says "remove class when click reaches background"

jQuery(document).ready(function($){
    //if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
    var MqL = 1134;
    var stickyHeader = 300;
    //move nav element position according to window width

    moveNavigation();
    if(checkWindowWidth()) {
        $('.cd-primary-nav').css('padding-right', ($(document).outerWidth() - $('.header-bottom-section .main').outerWidth()) / 2  - 40 + 'px');
        $('.header-search-wrapper').css('right', ($(document).outerWidth() - $('.header-bottom-section .main').outerWidth()) / 2 - 120 + 'px');
    }

    $(window).on('resize', function(){
        // if (!window.requestAnimationFrame) {
        //     setTimeout(moveNavigation, 300);
        // } else {
        //     window.requestAnimationFrame(moveNavigation);
        // }
        if(checkWindowWidth()) {
            $('.cd-primary-nav').css('padding-right', ($(document).outerWidth() - $('.header-bottom-section .main').outerWidth()) / 2 + 'px');
            $('.header-search-wrapper').css('right', ($(document).outerWidth() - $('.header-bottom-section .main').outerWidth()) / 2 - 80 + 'px');
        } else {
            $('.header-search-wrapper').css('right', '5%');
        }
    });

    $(window).scroll(function() {
        clearTimeout($.data(this, 'scrollTimer'));
        var scroll = getCurrentScroll();

        $.data(this, 'scrollTimer', setTimeout(function () {
            // do something
            if (scroll >= stickyHeader) {
                $('.page-header-sticky').addClass('navbar-fixed-top');
                closeNav();
                $('.cd-overlay').removeClass('is-visible');
            }
        }, 250));
        if (scroll < 50) {
            closeNav();
            $('.page-header-sticky').removeClass('navbar-fixed-top');
            $('.cd-overlay').removeClass('is-visible');
        }
    });

    //mobile - open lateral menu clicking on the menu icon
    $('.cd-nav-trigger').on('click', function(event){
        event.preventDefault();
        if( $('.cd-main-content').hasClass('nav-is-visible') ) {
            closeNav();
            $('.cd-overlay').removeClass('is-visible');
        } else {
            $(this).addClass('nav-is-visible');
            $('.cd-primary-nav').addClass('nav-is-visible');
            $('.cd-main-header').addClass('nav-is-visible');
            $('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                $('body').addClass('overflow-hidden');
            });
            toggleSearch('close');
            $('.cd-overlay').addClass('is-visible');
        }
    });
    //open search form
    $('.header-search-wrapper').on('click', function(event){
        event.preventDefault();
        event.stopPropagation();
        toggleSearch();
        closeNav();
    });
    $('html').on('click', function(event){
        if($('.header-search-wrapper').hasClass('active')) {
            toggleSearch('close');
        }
    });

    //close lateral menu on mobile
    $('.cd-overlay').on('swiperight', function(){
        if($('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            $('.cd-overlay').removeClass('is-visible');
        }
    });

    $('.nav-on-left .cd-overlay').on('swipeleft', function(){
        if($('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            $('.cd-overlay').removeClass('is-visible');
        }
    });

    $('.cd-overlay').on('click', function(){
        closeNav();
        toggleSearch('close');
        $('.cd-overlay').removeClass('is-visible');
    });

    //prevent default clicking on direct children of .cd-primary-nav
    $('.cd-primary-nav').children('.has-children').children('a').on('click', function(event){
        event.preventDefault();
    });

    //open submenu
    $('.has-children').children('a').on('click', function(event){
        if( !checkWindowWidth() ) event.preventDefault();
        var selected = $(this);
        if( selected.next('ul').hasClass('is-hidden') ) {
            //desktop version only
            selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
            selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
            $('.cd-overlay').addClass('is-visible');
        } else {
            selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
            $('.cd-overlay').removeClass('is-visible');
        }
        toggleSearch('close');
    });

    $('footer input').focusout(function() {
        $(this).parent().removeClass('focus');
    });
    $('footer input').focus(function() {
        $(this).parent().addClass('focus');
    });

    $('footer input').on('input', function() {
        var $field = $(this);
        if (this.value) {
            $field.addClass('has-value');
        } else {
            $field.removeClass('has-value');
        }
    });

    //submenu items - go back link
    $('.go-back').on('click', function(){
        $(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
    });

    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
    function closeNav() {
        $('.cd-nav-trigger').removeClass('nav-is-visible');
        $('.cd-main-header').removeClass('nav-is-visible');
        $('.cd-primary-nav').removeClass('nav-is-visible');
        $('.has-children ul').addClass('is-hidden');
        $('.has-children a').removeClass('selected');
        $('.moves-out').removeClass('moves-out');
        $('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
            $('body').removeClass('overflow-hidden');
        });
    }

    function toggleSearch(type) {
        if(type=="close") {
            //close search
            $('.header-search-wrapper').removeClass('active');
        } else {
            //open search
            $('.header-search-wrapper').addClass('active');
        }
    }

    function checkWindowWidth() {
        //check window width (scrollbar included)
        var e = window,
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
            return true;
        } else {
            return false;
        }
    }

    function moveNavigation(){
        var navigation = $('.cd-nav');
        var desktop = checkWindowWidth();
        if ( desktop ) {
            navigation.detach();
            navigation.insertBefore('.cd-header-buttons');
        } else {
            navigation.detach();
            navigation.insertAfter('.cd-main-content');
        }
    }
});